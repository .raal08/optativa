-------------------------------------------------------------
<!--css en html atributo style-->


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Erase una vez HTML</title>
</head>
<body style="color: #333; background-color: #ccc;">
    <h1 style="font-family: Georgia; font-size: 3em;" >Erase una vex HTML</h1>

    <h2 style="font-family: Georgia; font-size: 2em;">El padre</h2>
    <p style="font-family: Arial, Helvetica, sans-serif;">El padre de la web, <strong> Tim Berners-Lee</strong>, es tambie el padre del lenguaje HTML,
    <em>Hypertext Markup Language</em>.</p>
    <h2 style="font-family: Georgia; font-size: 2em;">La estandarización</h2>

    <p style="font-family: Arial, Helvetica, sans-serif;">EL lenguaje HTML está estandarizado por W2C, el <em>World Wide Web Conaortium</em>.</p>
    
    <p style="font-family: Arial, Helvetica, sans-serif;">La version actual de HTML es la 5, aunque no está completada.</p>

    <p style="font-family: Arial, Helvetica, sans-serif;">Se espera que esté terminada en el <strong>año 2014</strong>.</p>

</body>
</html>

---------------------------------------------------------------------------------------------------------------------------------
<!-- css en html con etiqueta style -->
 
 <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Erase una vez HTML</title>
    <style type="text/css">
    body {
        background-color: #ccc;
        color: #333;
    }

    h1, h2{
        font-family: Georgia;
    }
    
    h1 {
        font-size: 3em;
    }

    h2 {
        font-size: 2em;
    }

    p{
        font-family: Arial, Helvetica, sans-serif;
    }

    </style>
</head>
<body>
    <h1>Erase una vex HTML</h1>

    <h2>El padre</h2>
    <p>El padre de la web, <strong> Tim Berners-Lee</strong>, es tambie el padre del lenguaje HTML,
    <em>Hypertext Markup Language</em>.</p>
    <h2>La estandarización</h2>

    <p>EL lenguaje HTML está estandarizado por W2C, el <em>World Wide Web Conaortium</em>.</p>
    
    <p>La version actual de HTML es la 5, aunque no está completada.</p>
    
    <p>Se espera que esté terminada en el <strong>año 2014</strong>.</p>


</body>
</html>
----------------------------------------------------------------------
<!archivo html>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Erase una vez HTML</title>
   <link rel="stylesheet" href="style.css" type="text/css"/>
</head>
<body>
    <h1>Erase una vex HTML</h1>

    <h2>El padre</h2>
    <p>El padre de la web, <strong> Tim Berners-Lee</strong>, es tambie el padre del lenguaje HTML,
    <em>Hypertext Markup Language</em>.</p>
    <h2>La estandarización</h2>

    <p>EL lenguaje HTML está estandarizado por W2C, el <em>World Wide Web Conaortium</em>.</p>
    
    <p>La version actual de HTML es la 5, aunque no está completada.</p>
    
    <p>Se espera que esté terminada en el <strong>año 2014</strong>.</p>


</body>
</html>



------------------------
<!--archivo css-->


css 
    body {
        background-color: #ccc;
        color: #333;
    }

    h1, h2{
        font-family: Georgia;
    }
    
    h1 {
        font-size: 3em;
    }

    h2 {
        font-size: 2em;
    }

    p{
        font-family: Arial, Helvetica, sans-serif;
    }


